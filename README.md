# Compare  Shopping

#### 介绍

##### 基于JavaWeb的比价商城

#### 软件架构
```
菜鸟学习项目:
    基于servlet开发的比价商城项目
    分为商城前台和后台管理模块
    并未实现购物功能，一个纯粹的购物比价平台
    适合新手参考学习
```

#### 安装教程

```
1.  本项目jdk版本采用1.8，tomcat8.5.11版本
2.  将项目克隆下来，将doc文件夹下的sql文件导入本地数据并修改数据库配置信息
3.  运行截图
```

![avatar](doc/index.png)
![avatar](doc/index2.png)
![avatar](doc/商品管理.png)



#### 参与贡献
```
    1.  Fork 本仓库
    2.  新建 Feat_xxx 分支
    3.  提交代码
    4.  新建 Pull Request
```



#### 码云特技
```
    1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
    2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
    3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
    4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
    5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
    6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
```
