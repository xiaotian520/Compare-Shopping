package com.bijia.web.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import com.bijia.domain.User;
import com.bijia.service.impl.UserServiceImpl;
@WebServlet("/AdminUpdateUserServlet")
public class AdminUpdateUserServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			//设置请求的编码类型来解决post请求的乱码问题
			request.setCharacterEncoding("utf-8");
			
			//1.把表单的数据封装成模型
			User user = new User();
			try {
				BeanUtils.populate(user, request.getParameterMap());
			//2.更新数据
				
				UserServiceImpl userService = new UserServiceImpl();
				userService.updateUser(user);
			//3.回到list页面
				
			List<User> users = userService.findAllUsers();
			request.setAttribute("user", users);
			}  catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

}
