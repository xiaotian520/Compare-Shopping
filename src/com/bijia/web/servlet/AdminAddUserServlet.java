package com.bijia.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import com.bijia.domain.User;
import com.bijia.service.impl.UserServiceImpl;

@WebServlet("/AdminAddUserServlet")
public class AdminAddUserServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//设置请求的编码类型来解决post请求的乱码问题
		request.setCharacterEncoding("utf-8");
		
		//1.把表单的数据封装成模型
		User user = new User();
		try {
			BeanUtils.populate(user, request.getParameterMap());
			
			//2.调用service
			UserServiceImpl userService = new UserServiceImpl();
			userService.addUser(user);
			
			//3.返回list列表页面、重新获取数据
			List<User> users = userService.findAllUsers();
			request.setAttribute("users", users);
			request.getRequestDispatcher("./admin/member-list.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		} 
				
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
