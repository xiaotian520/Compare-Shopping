package com.bijia.web.servlet;

/**
 * 06.登录注册注销案例-提示注册用户已经存在
 */
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import com.bijia.domain.User;
import com.bijia.form.UserForm;
import com.bijia.service.IUserService;
import com.bijia.service.impl.UserServiceImpl;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		//1.判断jsp提交的表单参数是否都合法
		UserForm uf = new UserForm();
		//获取服务端和客户端传进来的vilidatecode;
		String client_code = request.getParameter("code");//客户端的验证码
		//2.获取服务端的code
		String server_code = (String) request.getSession().getAttribute("code");
		try {
			//把请求参数封装成表单校验对象
			BeanUtils.populate(uf, request.getParameterMap());
			//进行检验
			if(!uf.validate()){//不合法
				request.setAttribute("uf",uf);
				//回到注册页面
				request.getRequestDispatcher("/register.jsp").forward(request, response);
				return;
			}	
		} catch (IllegalAccessException | InvocationTargetException e1) {
			e1.printStackTrace();
		}
		
		
		//2.把请求参数封装成模型
		User user = new User();
		try {
			BeanUtils.populate(user, request.getParameterMap());
			
			//2.调用service
			IUserService userService = new UserServiceImpl();
			//判断数据库是否有重复用户名
			if(userService.userExist(user.getUsername())){
				//回到注册页面，给个提示
				uf.getErr().put("username", "用户名已经存在");
				request.setAttribute("uf", uf);
				request.getRequestDispatcher("/register.jsp").forward(request, response);
				return;
			}
			
			//验证码对比
			if(!server_code.equalsIgnoreCase(client_code)){
				//3.验证码对比
				request.setAttribute("errMsg","验证码错误");
				request.getRequestDispatcher("/register.jsp").forward(request, response);
			}else{
				//注册
				userService.register(user);
				//响应客户端
				response.setHeader("content-type", "text/html;charset=utf-8");
				response.getWriter().write("注册成功，3秒后进入登录页面");
			}
			//重定向sendRedirect方法的时候，要加项目名称+request.getContextPath()
			response.addHeader("Refresh", "3;url="+request.getContextPath()+"/login.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		} 
	
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);
	}

}
