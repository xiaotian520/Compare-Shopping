package com.bijia.web.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bijia.service.impl.CommdServiceImpl;
/**
 * 管理员后台删除商品
 * @author XiaoTian
 *
 */
@WebServlet("/DeleteCommdByIdServlet")
public class AdminDeleteCommdByIdServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//1.获取id
		String id = request.getParameter("id");
		//2.查找商品
		CommdServiceImpl commdService= new CommdServiceImpl();
		try {
			commdService.deleteCommdById(Integer.parseInt(id));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//3.重新加载list.jsp
		try {
			request.setAttribute("commds", commdService.findAllCommds());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.getRequestDispatcher("/CommdListServlet").forward(request, response);
	}
}
