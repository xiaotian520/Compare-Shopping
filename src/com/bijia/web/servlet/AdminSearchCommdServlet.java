package com.bijia.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bijia.domain.Commd;
import com.bijia.service.impl.CommdServiceImpl;
/**
 * 管理员后台通过名字、价格区间查找商品
 * @author XiaoTian
 *
 */
@WebServlet("/SearchCommdServlet")
public class AdminSearchCommdServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		//设置请求的编码类型来解决post请求的乱码问题
		request.setCharacterEncoding("utf-8");
		
		//接收参数
		String name = request.getParameter("name");
		String minprice = request.getParameter("minprice");
		String maxprice = request.getParameter("maxprice");
		
		//调用service 【根据条件查询】
		CommdServiceImpl commdService = new CommdServiceImpl();
		List<Commd> commds = commdService.findCommd(name,minprice,maxprice);
		
		//返回结果给list2.jsp
		request.setAttribute("commd", commds);
		request.getRequestDispatcher("./admin/bijia-list2.jsp").forward(request, response);
		
	}
}
