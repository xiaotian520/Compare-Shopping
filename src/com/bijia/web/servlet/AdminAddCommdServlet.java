package com.bijia.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import com.bijia.domain.Commd;
import com.bijia.service.impl.CommdServiceImpl;
/**
 * 管理员后台增加商品
 * @author XiaoTian
 *
 */
@WebServlet("/AddCommdServlet")
public class AdminAddCommdServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		//设置请求的编码类型来解决post请求的乱码问题
		request.setCharacterEncoding("utf-8");
		
		//1.把表单的数据封装成模型
		Commd commd = new Commd();
		try {
			BeanUtils.populate(commd, request.getParameterMap());
			
			//2.调用service
			CommdServiceImpl commdService = new CommdServiceImpl();
			commdService.addCommd(commd);
			
			//3.返回list列表页面、重新获取数据
			List<Commd> commds = commdService.findAllCommds();
			request.setAttribute("commds", commds);
			//request.getRequestDispatcher("./admin/add.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		
	}
}
