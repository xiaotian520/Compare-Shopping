package com.bijia.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bijia.domain.Commd;
import com.bijia.service.impl.CommdServiceImpl;
/**
 * 点击商品进行查找的servlet
 * @author XiaoTian
 *
 */
@WebServlet("/CompareServlet")
public class CompareServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置请求的编码类型来解决post请求的乱码问题
		request.setCharacterEncoding("utf-8");
		//1.接收搜索参数
		String name = request.getParameter("search");
		//2.调用业务方法
		
		//查出最便宜的一个
		CommdServiceImpl commdService = new CommdServiceImpl();
		List<Commd> commd = commdService.CompareCommd(name);
		
		//查出四个商城的货物
		List<Commd> commds = commdService.SearchCommd(name);
		//3.把数据放在请求对象中
		
		request.setAttribute("commd", commd);
		request.setAttribute("commds", commds);
		request.setAttribute("errMsg", "呜呜呜~<br>没有查到相应商品<br>换个词试试吧！");
		//4.进入/commd.jsp显示商品
		request.getRequestDispatcher("/commd.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
