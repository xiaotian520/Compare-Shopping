package com.bijia.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bijia.service.impl.CommdServiceImpl;
import com.bijia.service.impl.UserServiceImpl;

@WebServlet("/AdminDeleteUserByIdServlet")
public class AdminDeleteUserByIdServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			//1.获取id
			String id = request.getParameter("id");
			
			//2.查找商品
			UserServiceImpl userService= new UserServiceImpl();
			userService.deleteUserById(Integer.parseInt(id));
			
			//3.重新加载list.jsp
			request.setAttribute("commds", userService.findAllUsers());
			request.getRequestDispatcher("/AdminUserListServlet").forward(request, response);
		}
		

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
