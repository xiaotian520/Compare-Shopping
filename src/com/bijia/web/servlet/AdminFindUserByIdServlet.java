package com.bijia.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bijia.domain.User;
import com.bijia.service.impl.UserServiceImpl;

/**
 * Servlet implementation class AdminFindUserByIdServlet
 */
@WebServlet("/AdminFindUserByIdServlet")
public class AdminFindUserByIdServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//1.获取jsp中的id
		String id = request.getParameter("id");
		
		//2.查找id
		UserServiceImpl userService= new UserServiceImpl();
		User user = userService.findUserById(Integer.parseInt(id));
		
		//3.放在请求对象
		request.setAttribute("user", user);
		//4.转发到编辑页面进行编辑
		request.getRequestDispatcher("/admin/member-edit.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
