package com.bijia.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.dsna.util.images.ValidateCode;

/**
 * 验证码的Servlet
 */
@WebServlet("/ValidateCodeServlet")
public class ValidateCodeServlet extends HttpServlet {
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//1.生成验证
		ValidateCode vc = new ValidateCode(100, 30, 4, 6);

		HttpSession session = request.getSession();
		
		//2.把验证码存在session
		session.setAttribute("code", vc.getCode());
		
		//设置session的存活时间
		request.getSession().setMaxInactiveInterval(30);
		vc.write(response.getOutputStream());
	}

	

}
