package com.bijia.web.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

import com.bijia.dao.impl.UserDaoImpl;
import com.bijia.domain.User;
import com.bijia.exception.UserException;
import com.bijia.service.IUserService;
import com.bijia.service.impl.UserServiceImpl;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
   
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setHeader("content-type", "text/html;charset=utf-8");
		request.setCharacterEncoding("UTF-8");

		//1.获取请求参数
		User formUser = new User();
		UserDaoImpl userdao = new UserDaoImpl();
		try {
			BeanUtils.populate(formUser, request.getParameterMap());
			
			
			//2.调用业务方法(login)
			IUserService userService = new UserServiceImpl();
			
			try {
				User dbUser = userService.login(formUser);
				
				//登录用户存储到session
				request.getSession().setAttribute("user", dbUser);
				
				//进入后台页面
				String username = formUser.getUsername();
				formUser = userdao.findType(username);
				int type = formUser.getType();
				HttpSession session = request.getSession();
				session.setAttribute("login",username);
				if(type==1){
					response.sendRedirect(request.getContextPath() + "/admin/index.jsp");
				}else{
					response.sendRedirect(request.getContextPath() + "/index.jsp");
				}
				
			} catch (UserException e) {
				e.printStackTrace();
				//往request存数据
				request.setAttribute("message", e.getMessage());
				
				//登录失败,回到登录页面【转发】
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			}
			
			
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}

}
