package com.bijia.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bijia.domain.Commd;
import com.bijia.service.impl.CommdServiceImpl;
/**
 * 通过搜索框搜索商品查找的servlet
 * @author XiaoTian
 *
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//设置请求的编码类型来解决post请求的乱码问题
		request.setCharacterEncoding("utf-8");
		
		//接收参数
		String name = request.getParameter("search");
		
		//调用service 【根据条件查询商品】
		CommdServiceImpl CommdService = new CommdServiceImpl();
		List<Commd> commds = CommdService.SearchCommd(name);
		
		//返回结果给search.jsp
		request.setAttribute("Commds", commds);
		request.setAttribute("errMsg", "呜呜呜~<br>没有查到相应商品<br>换个词试试吧！");
		
		request.getRequestDispatcher("/search.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
