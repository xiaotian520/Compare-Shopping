package com.bijia.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bijia.domain.Commd;
import com.bijia.domain.PageResult;
import com.bijia.service.impl.CommdServiceImpl;
/**
 *  显示页码
 * @author XiaoTian
 *
 */

@WebServlet("/CommdListServlet")
public class AdminCommdListServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//1.获取页码
		String page = request.getParameter("page");
		//如果没有传值就显示第1页的数据
		if(page == null || "".equals(page)){
			page = "1";
		}
		
		//调用业务方法
		CommdServiceImpl commdService = new CommdServiceImpl();
		PageResult<Commd> pageResult = commdService.findCommdsByPage(Integer.parseInt(page));
		
		//2.把数据放在请求对象中
		request.setAttribute("pageResult", pageResult);
		
		//3.进入/admin/bijia-list.jsp
		request.getRequestDispatcher("/admin/bijia-list.jsp").forward(request, response);
		
	}

	

}
