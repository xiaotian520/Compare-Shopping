package com.bijia.web.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bijia.domain.User;
import com.bijia.service.impl.UserServiceImpl;

@WebServlet("/AdminSearchUserServlet")
public class AdminSearchUserServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置请求的编码类型来解决post请求的乱码问题
		request.setCharacterEncoding("utf-8");
		
		//接收参数
		String username = request.getParameter("username");
		String type = request.getParameter("type");
		
		//调用service 【根据条件查询】
		UserServiceImpl userService = new UserServiceImpl();
		List<User> users = null;
		try {
			users = userService.findUsers(username,type);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//返回结果给list2.jsp
		request.setAttribute("users", users);
		request.getRequestDispatcher("./admin/member-list2.jsp").forward(request, response);
		
	}
		

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
