package com.bijia.web.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bijia.domain.Commd;
import com.bijia.service.impl.CommdServiceImpl;
/**
 * 管理员后台通过id查找需要修改的商品
 * @author XiaoTian
 *
 */
@WebServlet("/FindCommdByIdServlet")
public class AdminFindCommdByIdServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//1.获取jsp中的id
		String id = request.getParameter("id");
		
		//2.查找id
		CommdServiceImpl commdService= new CommdServiceImpl();
		Commd commd = null;
		try {
			commd = commdService.findCommdById(Integer.parseInt(id));
		} catch (NumberFormatException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//3.放在请求对象
		request.setAttribute("commd", commd);
		//4.转发到编辑页面进行编辑
		request.getRequestDispatcher("/admin/bijia-edit.jsp").forward(request, response);
	}
}
