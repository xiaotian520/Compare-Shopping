package com.bijia.web.servlet;
/**
 * 显示商品
 */
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bijia.domain.Commd;
import com.bijia.service.impl.CommdServiceImpl;

@WebServlet("/ShowCommdServlet")
public class ShowCommdServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//设置请求的编码类型来解决post请求的乱码问题
		request.setCharacterEncoding("utf-8");
		//1.调用业务方法
		CommdServiceImpl commdService = new CommdServiceImpl();
		List<Commd> commds = commdService.ShowCommds();
		//3.把数据放在请求对象中
		request.setAttribute("commd", commds);
		//4.进入/commd.jsp显示商品
		request.getRequestDispatcher("/index.jsp").forward(request, response);

		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
