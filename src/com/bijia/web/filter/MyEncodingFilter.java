package com.bijia.web.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")//拦截所有请求
public class MyEncodingFilter implements Filter {

    @Override
    public void destroy() { }
    @Override
    public void init(FilterConfig filterConfig) throws ServletException { }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        //解决响应的乱码
        HttpServletResponse mResponse = (HttpServletResponse) response;
        mResponse.setHeader("content-type","text/html;charset=UTF-8");

        //设置POST请求参数中文乱码问题
        HttpServletRequest hsr = (HttpServletRequest)request;
        if (hsr.getMethod().equalsIgnoreCase("post")){
            request.setCharacterEncoding("UTF-8");
            System.out.println("拦截请求"+request);
        }
        chain.doFilter(request,response);
    }
}
