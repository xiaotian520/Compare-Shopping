package com.bijia.service;

import java.sql.SQLException;
import java.util.List;

import com.bijia.domain.Commd;
import com.bijia.domain.PageResult;

public interface ICommdService {

	public List<Commd> findAllCommds() throws SQLException;
	
	public List<Commd> SearchCommd(String name) throws SQLException;
	
	public List<Commd> CompareCommd(String name) throws SQLException;
	
	public PageResult<Commd> findCommdsByPage(int page) throws SQLException;

	public List<Commd> ShowCommds() throws SQLException;
}
