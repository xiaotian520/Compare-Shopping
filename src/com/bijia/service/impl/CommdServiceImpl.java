package com.bijia.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.bijia.dao.ICommdDao;
import com.bijia.dao.impl.CommdDaoImpl;
import com.bijia.domain.Commd;
import com.bijia.domain.PageResult;
import com.bijia.service.ICommdService;

public class CommdServiceImpl implements ICommdService{
	//service中调用dao
	private ICommdDao commdDao = new CommdDaoImpl();
	
	/**
	 * 查找所有的商品
	 * @throws SQLException 
	 */
	public List<Commd> findAllCommds() throws SQLException{
		return  commdDao.findAllCommds();
	}
	
	/**
	 * 添加商品
	 * @param commd
	 * @throws SQLException 
	 */
	public void addCommd(Commd commd) throws SQLException{
		commdDao.addCommd(commd);
	}
	
	/**
	 * 通过id找商品、为了进行编辑时显示数据
	 * @param id
	 * @return
	 * @throws SQLException 
	 */
	public Commd findCommdById(int id) throws SQLException{
		return commdDao.findCommdById(id);
	}
	
	/**
	 * 更新商品
	 * @param commd
	 * @throws SQLException 
	 */
	public void updateCommd(Commd commd) throws SQLException{
		commdDao.updateCommd(commd);
		
	}

/**
 * 删除商品
 * @param id
 */
	public void deleteCommdById(int id) throws SQLException {
		
		commdDao.deleteCommdById(id);
		
	}

/**
 * 通过商品名、价格查找商品
 * @param name
 * @param minprice
 * @param maxprice
 * @return
 */
	public List<Commd> findCommd( String name, String minprice, String maxprice) {
		try {
			return commdDao.findCommd(name, minprice, maxprice);
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		
		return null;
	}


	/**
	 * 分页查询
	 * @param page
	 * @return
	 */
	public PageResult<Commd> findCommdsByPage(int page){
		try {
			return commdDao.findCommdsByPage(page);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	/**
	 * 查找商品
	 */
	public List<Commd> SearchCommd(String name) {
		//1.创建dao对象
		CommdDaoImpl CommdDao = new CommdDaoImpl();
		//2.调用findAllCommd的方法
			try {
				return  CommdDao.SearchCommd(name);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	}
	
	/**
	 * 比较商品
	 */
	public List<Commd> CompareCommd(String name) {
		//1.创建dao对象
		CommdDaoImpl CommdDao = new CommdDaoImpl();
		//2.调用CompareCommd的方法
			try {
				return  CommdDao.CompareCommd(name);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	}

	/**
	 * 显示商品
	 */
	public List<Commd> ShowCommds() {
		//1.创建dao对象
		CommdDaoImpl CommdDao = new CommdDaoImpl();
		return  CommdDao.ShowCommds();
	}
	
}
