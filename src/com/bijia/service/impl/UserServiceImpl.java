package com.bijia.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.bijia.dao.IUserDao;
import com.bijia.dao.impl.UserDaoImpl;
import com.bijia.domain.PageResult;
import com.bijia.domain.User;
import com.bijia.exception.UserException;
import com.bijia.service.IUserService;

public class UserServiceImpl implements IUserService{

	//service中调用dao
	private IUserDao userDao = new UserDaoImpl();
	
	@Override
	public void register(User user) {
		userDao.insert(user);
	}

	@Override
	public User login(String username, String password) {
		return userDao.findUser(username, password);
	}

	@Override
	public User login(User user)throws UserException {
		User u = userDao.findUser(user.getUsername(), user.getPassword());
		if(u != null){
			return u;
		}else{
			throw new UserException("用户名或密码不正确");
		}
		
	}

	@Override
	public boolean userExist(String username) {
		User user = userDao.findUser(username);
		return user != null;
	}

	@Override
	public User login(String username) {
		return userDao.findType(username);
	}
	

	/**
	 * 分页查询
	 * @param page
	 * @return
	 */
	public PageResult<User> findUsersByPage(int page){
		try {
			return userDao.findUsersByPage(page);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	/**
	 * 添加用户
	 * @param user
	 */
	public void addUser(User user) throws SQLException{
		
		userDao.insert(user);
	}
	
	/**
	 * 通过id找商品、为了进行编辑时显示数据
	 * @param id
	 * @return
	 */
	public User findCommdById(int id){
		try {
			return userDao.findUserById(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * 更新商品
	 * @param commd
	 */
	public void updateUser(User user){
		try {
			userDao.updateUser(user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

/**
 * 删除商品
 * @param id
 */
	public void deleteUserById(int id) {
		
		try {
			userDao.deleteUserById(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

/**
 * 通过用户名、用户类型查找用户
 * @param name
 * @return
 * @throws SQLException 
 */
	public List<User> findUsers(String name,String type) throws SQLException {
		try {
			return userDao.findUsers(name,type);
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		
		return null;
	}

	/**
	 * 查找所有的用户
	 */
	public List<User> findAllUsers(){
		//1.创建dao对象
		//2.调用findAll的方法
		try {
			return  userDao.findAllUsers();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	/**
	 * 通过id找用户、为了进行编辑时显示数据
	 * @param id
	 * @return
	 */
	public User findUserById(int id){
		try {
			return userDao.findUserById(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
