package com.bijia.domain;

public class Commd {

	private int id;// 商品的id
	private String name;// 商品名
	private double price;// 商品价格
	private String img;// 商品的图片
	private String details;// 描述
	private String family;//分类商城
	private String logo;//商城logo
	private String link;//商城链接
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family = family;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	@Override
	public String toString() {
		return "Commd [id=" + id + ", name=" + name + ", price=" + price + ", img=" + img + ", details=" + details
				+ ", family=" + family + ", logo=" + logo + ", link=" + link + "]";
	}
	
}
