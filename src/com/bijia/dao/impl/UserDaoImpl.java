package com.bijia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.bijia.dao.IUserDao;
import com.bijia.domain.PageResult;
import com.bijia.domain.User;
import com.bijia.utils.C3P0Utils;
import com.bijia.utils.MD5Utils;

public class UserDaoImpl implements IUserDao {

	@Override
	public void insert(User user) {
		// TODO Auto-generated method stub
		Connection conn = null;
		PreparedStatement ps = null;

		// 写JDBC代码
		try {
			// 1.注册驱动
			// 2.获取connection对象
			conn = C3P0Utils.getConnection();

			// 3.获取Statement对象
			String sql = "insert into t_user (username,password,email,type) values(?,?,?,?)";
			ps = conn.prepareStatement(sql);

			// 设置参数
			ps.setString(1, user.getUsername());
			ps.setString(2, MD5Utils.md5( user.getPassword()));
			ps.setString(3, user.getEmail());
			ps.setInt(4, 0);
			
			// 4.执行sql语句 delete/update/insert
			int r = ps.executeUpdate();
			System.out.println("受影响的结果行数:" + r);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// 5.关闭资源
			C3P0Utils.close(null, ps, conn);
		}
	}

	@Override
	public User findUser(String username, String password) {
		User user = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		// 写JDBC代码
		try {
			// 1.注册驱动
			// 2.获取connection对象
			conn = C3P0Utils.getConnection();

			// 3.查询
			String sql = "select * from t_user where username = ? and password = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, MD5Utils.md5(password));
			
			rs = ps.executeQuery();
			
			//4.遍历
			while(rs.next()){
				user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setEmail(rs.getString("email"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 5.关闭资源
			C3P0Utils.close(rs, ps, conn);
		}
		
		return user;
	}

	@Override
	public User findUser(String username) {
		User user = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		// 写JDBC代码
		try {
			// 1.注册驱动
			// 2.获取connection对象
			conn = C3P0Utils.getConnection();

			// 3.查询
			String sql = "select * from t_user where username = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			rs = ps.executeQuery();
			
			//4.遍历
			while(rs.next()){
				user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setEmail(rs.getString("email"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// 5.关闭资源
			C3P0Utils.close(rs, ps, conn);
		}
		return user;
	}

	@Override
	public User findType(String username) {
		User user = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		// 写JDBC代码
		try {
			// 1.注册驱动
			// 2.获取connection对象
			conn = C3P0Utils.getConnection();

			// 3.查询
			String sql = "select type from t_user where username = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			rs = ps.executeQuery();
			//4.遍历
			while(rs.next()){
				user = new User();
				user.setType(rs.getInt("type"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 5.关闭资源
			C3P0Utils.close(rs, ps, conn);
		}
		return user;
	}
	

	/**
	 * 根据当前页返回数据
	 * @param page 查询的页码
	 * @return
	 * @throws SQLException 
	 */
	public PageResult<User> findUsersByPage(int page) throws SQLException{
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		//1.创建PageResult对象
		PageResult<User> pr = new PageResult<User>();
		
		//2.设置totalCount【总记录数】
		Long totalCount = (Long) qr.query("select COUNT(*) from t_user", new ScalarHandler());
		pr.setTotalCount(totalCount);
		
		//3.设置总页数 10/5
		int totalPage = (int)(totalCount % pr.getPageCount() == 0 ? totalCount / pr.getPageCount() : totalCount / pr.getPageCount() + 1);
		pr.setTotalPage(totalPage);
		
		//4.设置 当前页
		pr.setCurrentPage(page);
		
		//5.设置pageresult里的list数据【库存排序】
		String sql = "select * from t_user order by id limit ?,?";
		int start = (page - 1) * pr.getPageCount();
		List<User> users = qr.query(sql, new BeanListHandler<User>(User.class),start,pr.getPageCount());
		pr.setList(users);
		
		return pr;
	}

	/**
	 * 更新数据
	 * @throws SQLException 
	 */
	public void updateUser(User user) throws SQLException{
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "update t_user set username = ?, password = ? , email = ? , type= ? where id = ?";
		qr.update(sql, user.getUsername(),user.getPassword(),user.getEmail(),user.getType(),user.getId());
	}
	
	/**	
	 * 删除
	 * @param id
	 * @throws SQLException
	 */
	public void deleteUserById(int id) throws SQLException {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		String sql = "delete from t_user where id = ?";
		
		qr.update(sql,id); 
	}
	
	public List<User> findUser(String name,String minprice, String maxprice) throws SQLException {
		
		//查询
		//1.拼接sql语句
		String sql = "select * from jing_shop where 1=1";
		
		if(!"".equals(name)){
			sql += " and name like '%" + name +"%'";
		}
		//价格
		if(!"".equals(minprice)){
			sql += " and price >= " + minprice;
		}
		
		if(!"".equals(maxprice)){
			sql += " and price <= " + maxprice;
		}
		
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		return qr.query(sql, new BeanListHandler<User>(User.class));
	}
	
	public List<User> findAllUsers() throws SQLException {
		//1.创建queryrunner
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		return qr.query("select * from t_user", new BeanListHandler<User>(User.class));
		
	}
	
	/**
	 * 通过id找到用户
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public User findUserById(int id) throws SQLException{
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		String sql = "select * from t_user where id=?";
		
		return qr.query(sql, new BeanHandler<User>(User.class),id);
		
	}

	@Override
	public List<User> findUsers(String name,String type) throws SQLException {
		//查询
		//1.拼接sql语句
		String sql = "select * from t_user where 1=1";
		
		if(!"".equals(name)){
			sql += " and username like '%" + name +"%'";
		}
		
		//价格
		if(!"".equals(type)){
			sql += " and type = " + type;
		}
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		return qr.query(sql, new BeanListHandler<User>(User.class));
	}
}
