package com.bijia.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.bijia.dao.ICommdDao;
import com.bijia.domain.Commd;
import com.bijia.domain.PageResult;
import com.bijia.utils.C3P0Utils;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;

public class CommdDaoImpl implements ICommdDao{

	PreparedStatement pstmt = null;
	Connection conn = null;
	ResultSet rs = null;
	
	@Override
	public List<Commd> findAllCommd() throws SQLException {
		//1.创建queryrunner
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		return qr.query("select * from jing_shop  union all select * from yams_shop union all select * from mao_shop union all select * from su_shop", new BeanListHandler<Commd>(Commd.class));
		
	}
	//查找对比商品
public List<Commd> CompareCommd(String name) throws SQLException {
		
		//查询
		//1.拼接sql语句
		String sql = "select * from (select * from jing_shop  union all select * from yams_shop union all select * from mao_shop union all select * from su_shop) t where 1=1";
		if(!"".equals(name)){
			sql += " and name like '%" + name +"%' order by price limit 0,1";
		}
		
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		return qr.query(sql, new BeanListHandler<Commd>(Commd.class));
	}
	//搜索框search查找商品
public List<Commd> SearchCommd(String name) throws SQLException {
		
		//查询
		//1.拼接sql语句
		String sql = "select * from (select * from jing_shop  union all select * from yams_shop union all select * from mao_shop union all select * from su_shop) t where 1=1";
		if(!"".equals(name)){
			sql += " and name like '%" + name +"%' order by price";
		}else{
			sql+=" and name like '%" + null +"%' order by price";
		}
		
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		return qr.query(sql, new BeanListHandler<Commd>(Commd.class));
	}
	
	public List<Commd> findAllCommds() throws SQLException{
		//1.创建queryrunner
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		return qr.query("select * from jing_shop", new BeanListHandler<Commd>(Commd.class));
	}
	
	public void addCommd(Commd commd)throws SQLException{
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		String sql = "insert into jing_shop(name,price,img,details,family) values (?,?,?,?,?)";
		
		//定义一个数组参数
		Object[] params = new Object[5];
		params[0] = commd.getName();
		params[1] = commd.getPrice();
		params[2] = commd.getImg();
		params[3] = commd.getDetails();
		params[4] = commd.getFamily();
		qr.update(sql,params);
		
	}
	
	/**
	 * 通过id找到商品
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public Commd findCommdById(int id) throws SQLException{
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		String sql = "select * from jing_shop where id=?";
		
		return qr.query(sql, new BeanHandler<Commd>(Commd.class),id);
		
	}
	
	/**
	 * 更新数据
	 * @throws SQLException 
	 */
	public void updateCommd(Commd commd) throws SQLException{
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "update jing_shop set name = ?, price = ? , img = ? , details = ? , family = ? where id = ?";
		qr.update(sql, commd.getName(),commd.getPrice(),commd.getImg(),commd.getDetails(),commd.getFamily(),commd.getId());
	}
/**
 * 删除
 * @param id
 * @throws SQLException
 */
	public void deleteCommdById(int id) throws SQLException {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		String sql = "delete from jing_shop where id = ?";
		
		qr.update(sql,id); 
	}
	
	public List<Commd> findCommd(String name,String minprice, String maxprice) throws SQLException {
		
		//查询
		//1.拼接sql语句
		String sql = "select * from jing_shop where 1=1";
		
		if(!"".equals(name)){
			sql += " and name like '%" + name +"%'";
		}
		//价格
		if(!"".equals(minprice)){
			sql += " and price >= " + minprice;
		}
		
		if(!"".equals(maxprice)){
			sql += " and price <= " + maxprice;
		}
		
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		return qr.query(sql, new BeanListHandler<Commd>(Commd.class));
	}
	
	
	/**
	 * 根据当前页返回数据
	 * @param page 查询的页码
	 * @return
	 * @throws SQLException 
	 */
	public PageResult<Commd> findCommdsByPage(int page) throws SQLException{
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		
		//1.创建PageResult对象
		PageResult<Commd> pr = new PageResult<Commd>();
		
		//2.设置totalCount【总记录数】
		Long totalCount = (Long) qr.query("select COUNT(*) from jing_shop", new ScalarHandler());
		pr.setTotalCount(totalCount);
		
		//3.设置总页数 10/5
		int totalPage = (int)(totalCount % pr.getPageCount() == 0 ? totalCount / pr.getPageCount() : totalCount / pr.getPageCount() + 1);
		pr.setTotalPage(totalPage);
		
		//4.设置 当前页
		pr.setCurrentPage(page);
		
		//5.设置pageresult里的list数据【库存排序】
		String sql = "select * from jing_shop order by price limit ?,?";
		int start = (page - 1) * pr.getPageCount();
		List<Commd> commds = qr.query(sql, new BeanListHandler<Commd>(Commd.class),start,pr.getPageCount());
		pr.setList(commds);
		
		return pr;
	}
	public List<Commd> ShowCommds() {
		//查询
		//1.拼接sql语句
		String sql= "select * from jing_shop where name like '%冰箱%' or name like '%IPad%' or name like '%笔记本%' or name like '%手机%' or name like '%空调%' or name like '%电视%' or name like '%热水器%' or name like '%苹果%' limit 0,12";
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		System.out.println(sql);
		try {
			return qr.query(sql, new BeanListHandler<Commd>(Commd.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
