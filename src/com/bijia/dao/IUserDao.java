package com.bijia.dao;

import java.sql.SQLException;
import java.util.List;

import com.bijia.domain.PageResult;
import com.bijia.domain.User;

public interface IUserDao {

	/**
	 * 插入数据
	 * @param user
	 */
	public void insert(User user);
	
	/**
	 * 通过用户名和密码查找用户
	 * @param username
	 * @param password
	 * @return
	 */
	public User findUser(String username,String password);
	
	/**
	 * 通过用户名查询用户
	 * @param username
	 * @return
	 */
	public User findUser(String username);
	/**
	 * 通过type查询类型
	 * @param type
	 * @return
	 */
	public User findType(String username);

	/**
	 * 
	 * @param page
	 * @return
	 * @throws SQLException
	 */
	public PageResult<User> findUsersByPage(int page) throws SQLException;
	/**
	 * 删除用户
	 * @param id
	 * @throws SQLException 
	 */
	public void deleteUserById(int id) throws SQLException;
	/**
	 * 通过id查找用户
	 * @param id
	 * @return
	 * @throws SQLException 
	 */
	public User findUserById(int id) throws SQLException;
	/**
	 * 更新用户信息
	 * @param user
	 * @throws SQLException 
	 */
	public void updateUser(User user) throws SQLException;
	/**
	 * 
	 * @param name
	 * @param minprice
	 * @param maxprice
	 * @return
	 * @throws SQLException
	 */
	public List<User> findUser(String name, String minprice, String maxprice) throws SQLException;
	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public List<User> findAllUsers() throws SQLException;
	/**
	 * 
	 * @param name
	 * @param type
	 * @return
	 * @throws SQLException
	 */
	public List<User> findUsers(String name,String type) throws SQLException;
}
