package com.bijia.dao;

import java.sql.SQLException;
import java.util.List;

import com.bijia.domain.Commd;
import com.bijia.domain.PageResult;

public interface ICommdDao {
	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public List<Commd> findAllCommd() throws SQLException;
		/**
		 * 
		 * @param name
		 * @return
		 * @throws SQLException
		 */
	public List<Commd> SearchCommd(String name) throws SQLException;
		/**
		 * 
		 * @param name
		 * @return
		 * @throws SQLException
		 */
	public List<Commd> CompareCommd(String name) throws SQLException;
	/**
	 * 
	 * @param commd
	 * @throws SQLException
	 */
	public void addCommd(Commd commd) throws SQLException;
	/**
	 * 
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public Commd findCommdById(int id) throws SQLException;
	/**
	 * 	
	 * @return
	 * @throws SQLException
	 */
	public List<Commd> findAllCommds() throws SQLException;
	/**
	 * 
	 * @param commd
	 * @throws SQLException
	 */
	public void updateCommd(Commd commd) throws SQLException;
	/**
	 * 
	 * @param id
	 * @throws SQLException
	 */
	public void deleteCommdById(int id) throws SQLException;
	/**
	 * 
	 * @param name
	 * @param minprice
	 * @param maxprice
	 * @return
	 * @throws SQLException
	 */
	public List<Commd> findCommd(String name, String minprice, String maxprice) throws SQLException;
	/**
	 * 
	 * @param page
	 * @return
	 * @throws SQLException
	 */
	public PageResult<Commd> findCommdsByPage(int page) throws SQLException;

	
}
