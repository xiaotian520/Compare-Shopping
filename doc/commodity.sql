/*
Navicat MySQL Data Transfer

Source Server         : xiaotian
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : commodity

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-12-10 21:08:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jing_shop
-- ----------------------------
DROP TABLE IF EXISTS `jing_shop`;
CREATE TABLE `jing_shop` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` double(100,0) NOT NULL,
  `img` varchar(100) NOT NULL,
  `details` varchar(200) NOT NULL,
  `family` varchar(20) DEFAULT '京东商城',
  `logo` varchar(50) DEFAULT 'img/jindong.png',
  `link` varchar(200) DEFAULT 'https://www.jd.com',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100061 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jing_shop
-- ----------------------------
INSERT INTO `jing_shop` VALUES ('100001', '小米笔记本pro', '5699', 'img/xiaomi_air.jpg', '小米(MI)Pro 15.6英寸金属轻薄笔记本(i5-8250U 8G 256G PCIE SSD MX150 2G独显 72%NTSC FHD 指纹识别 预装office)深空灰', '京东商城', 'img/jindong.png', 'https://www.jd.com');
INSERT INTO `jing_shop` VALUES ('100002', '西门子/冰箱', '5499', 'img/east_bx.jpg', '西门子（SIEMENS）610升 变频风冷对开门冰箱 独立并联双循环 （白色）BCD-610W(KA92NV02TI)', '京东商城', 'img/jindong.png', 'https://www.jd.com');
INSERT INTO `jing_shop` VALUES ('100005', 'Xiaomi/小米手机', '2699', 'img/xiaomi_phone.jpg', 'Xiaomi/小米 小米mix 2全面屏5.99英寸128G拍照手机小米NFC手机MIX3S', '京东商城', 'img/jindong.png', 'https://www.jd.com');
INSERT INTO `jing_shop` VALUES ('100006', '奥克斯空调', '5699', 'img/akskt.jpg', '奥克斯（AUX）3匹 一级能效 变频冷暖 智能 倾城空调柜机 京东微联APP控制(KFR-72LW/BpNHA2+1)', '京东商城', 'img/jindong.png', 'https://www.jd.com');
INSERT INTO `jing_shop` VALUES ('100047', '小米IPad', '1099', 'img/xiaomi_ipd.jpg', 'Xiaomi/小米 小米平板4 大屏8英寸安卓智能平板电脑 AI识别 全高清', '京东商城', 'img/jindong.png', 'https://www.jd.com');
INSERT INTO `jing_shop` VALUES ('100051', 'Son/平板电视', '6999', 'img/sony_tv.jpg', 'Sony/索尼 KD-55X9000F 55英寸4K HDR高清智能网络平板液晶电视', '京东商城', 'img/jindong.png', 'https://www.jd.com');
INSERT INTO `jing_shop` VALUES ('100053', 'Haier/海尔/热水器', '1699', 'img/haier_water.jpg', 'Haier/海尔 EC8002-MC5电热水器80升家用速热储水洗澡卫生间智能', '京东商城', 'img/jindong.png', 'https://www.jd.com');
INSERT INTO `jing_shop` VALUES ('100055', '长虹/热水器', '468', 'img/changhong_water.jpg', 'Changhong/长虹 cz-8热水器电家用即热式小型淋浴卫生间速热洗澡', '京东商城', 'img/jindong.png', 'https://www.jd.com');
INSERT INTO `jing_shop` VALUES ('100058', 'Apple/苹果 iPhone X', '6828', 'img/iphonex.jpg', 'Apple/苹果 iPhone X 苹果x 苹果10 iphonex iphone xs max', '京东商城', 'img/jindong.png', 'https://www.jd.com');
INSERT INTO `jing_shop` VALUES ('100060', 'vivo Z3/手机', '1898', 'img/vivoz3.jpg', 'vivo Z3水滴全面屏高通骁龙710AIE处理器全网通智能4G限量版新款手机官方正品vivoz3 Z3', '京东商城', 'img/jindong.png', 'https://www.jd.com');

-- ----------------------------
-- Table structure for mao_shop
-- ----------------------------
DROP TABLE IF EXISTS `mao_shop`;
CREATE TABLE `mao_shop` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` double(100,0) NOT NULL,
  `img` varchar(100) NOT NULL,
  `details` varchar(200) NOT NULL,
  `family` varchar(10) DEFAULT '天猫商城',
  `logo` varchar(50) DEFAULT 'img/taobao.png',
  `link` varchar(200) DEFAULT 'https://www.tmall.com',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100058 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mao_shop
-- ----------------------------
INSERT INTO `mao_shop` VALUES ('100001', '小米笔记本pro', '5599', 'img/xiaomi_air.jpg', '小米(MI)Pro 15.6英寸金属轻薄笔记本(i5-8250U 8G 256G PCIE SSD MX150 2G独显 72%NTSC FHD 指纹识别 预装office)深空灰', '天猫商城', 'img/taobao.png', 'https://www.tmall.com');
INSERT INTO `mao_shop` VALUES ('100002', 'Midea/冰箱', '3199', 'img/midea_bx.jpg', '美的(Midea)258升 三门冰箱 变频无霜 一级能效 宽幅变温精准保鲜 电冰箱 睿智金 BCD-258WTPZM(E)', '天猫商城', 'img/taobao.png', 'https://www.tmall.com');
INSERT INTO `mao_shop` VALUES ('100006', '美的空调', '5399', 'img/mdkt.jpg', '美的（Midea）3匹 智行 静音 圆柱立柜式 定速冷暖 客厅空调柜机 KFR-72LW/DY-YA400(D3)', '天猫商城', 'img/taobao.png', 'https://www.tmall.com');
INSERT INTO `mao_shop` VALUES ('100046', 'Microsoft/微软 IPad', '7688', 'img/microsoft_ipd.jpg', 'Microsoft/微软 Surface Pro i5 8G 256G (第五代) 笔记本平板电脑二合一', '天猫商城', 'img/taobao.png', 'https://www.tmall.com');
INSERT INTO `mao_shop` VALUES ('100049', 'Skyworth/创维 平板电视', '7699', 'img/cw_tv.jpg', 'Skyworth/创维 75A7 75英寸彩电4KHDR超清智能网络平板液晶电视机', '天猫商城', 'img/taobao.png', 'https://www.tmall.com');
INSERT INTO `mao_shop` VALUES ('100054', 'Midea/美的/热水器', '1099', 'img/midea_water.jpg', 'Midea/美的 F60-15WB5(Y)60升电热水器卫生间洗澡速热家用储水式', '天猫商城', 'img/taobao.png', 'https://www.tmall.com');
INSERT INTO `mao_shop` VALUES ('100057', 'Apple/苹果 iPhone X ', '6488', 'img/iphone7p.jpg', 'Apple/苹果 iPhone X 苹果x 苹果10 iphonex iphone xs max', '天猫商城', 'img/taobao.png', 'https://www.tmall.com');

-- ----------------------------
-- Table structure for su_shop
-- ----------------------------
DROP TABLE IF EXISTS `su_shop`;
CREATE TABLE `su_shop` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` double(100,0) NOT NULL,
  `img` varchar(100) NOT NULL,
  `details` varchar(200) NOT NULL,
  `family` varchar(10) DEFAULT '苏宁易购',
  `logo` varchar(50) DEFAULT 'img/suning.png',
  `link` varchar(200) DEFAULT 'https://www.suning.com',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100059 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of su_shop
-- ----------------------------
INSERT INTO `su_shop` VALUES ('100001', '小米笔记本pro', '5699', 'img/xiaomi_air.jpg', '小米(MI)Pro 15.6英寸金属轻薄笔记本(i5-8250U 8G 256G PCIE SSD MX150 2G独显 72%NTSC FHD 指纹识别 预装office)深空灰', '苏宁易购', 'img/suning.png', 'https://www.suning.com');
INSERT INTO `su_shop` VALUES ('100002', 'Midea/冰箱', '3999', 'img/midea_bx2.jpg', '美的(Midea)对开门冰箱 525升 变频无霜 中央智控 智能节能电冰箱 星际银 BCD-525WKPZM(E)', '苏宁易购', 'img/suning.png', 'https://www.suning.com');
INSERT INTO `su_shop` VALUES ('100006', '奥克斯空调2P', '5399', 'img/aks2p.jpg', '奥克斯（AUX）2匹 冷暖 变频 立柜式空调柜机(KFR-51LW/BpNSP1+3)', '苏宁易购', 'img/suning.png', 'https://www.suning.com');
INSERT INTO `su_shop` VALUES ('100045', 'Huawei/华为/IPad', '3888', 'img/huawei_ipd.jpg', 'Huawei/华为 M5 Pro平板电脑10.8英寸 高清显示安卓WiFi/4G可通话二合一智能游戏电脑 ', '苏宁易购', 'img/suning.png', 'https://www.suning.com');
INSERT INTO `su_shop` VALUES ('100048', '小米/平板电视4A', '2799', 'img/xiaomi_tv.jpg', 'Xiaomi/小米 小米电视4A 55英寸 4k超高清智能网络电视机', '苏宁易购', 'img/suning.png', 'https://www.suning.com');
INSERT INTO `su_shop` VALUES ('100053', 'Haier/海尔/热水器2', '3699', 'img/haier_water2.jpg', 'Haier/海尔 EC8002-MC5电热水器80升家用速热储水洗澡卫生间智能', '苏宁易购', 'img/suning.png', 'https://www.suning.com');
INSERT INTO `su_shop` VALUES ('100058', 'Apple/苹果 iPhone X ', '6548', 'img/iphone6s.jpg', 'Apple/苹果 iPhone X 苹果x 苹果10 iphonex iphone xs max', '苏宁易购', 'img/suning.png', 'https://www.suning.com');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `type` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'xiaotian', 'e10adc3949ba59abbe56e057f20f883e', '446691726@qq.com', '0000000001');
INSERT INTO `t_user` VALUES ('2', 'zhang', 'e10adc3949ba59abbe56e057f20f883e', '446691726@qq.com', '0000000000');
INSERT INTO `t_user` VALUES ('3', 'test333', 'e10adc3949ba59abbe56e057f20f883e', 'ddddd@qq.com', '0000000000');
INSERT INTO `t_user` VALUES ('5', 'test', 'e10adc3949ba59abbe56e057f20f883e', '1234fdsf56@qq.com', '0000000000');
INSERT INTO `t_user` VALUES ('6', 'test2', 'e10adc3949ba59abbe56e057f20f883e', '123@qq.cpm', '0000000000');
INSERT INTO `t_user` VALUES ('10', 'xz123', 'e10adc3949ba59abbe56e057f20f883e', '123456789@qq.com', '0000000001');
INSERT INTO `t_user` VALUES ('11', 'test123', 'e10adc3949ba59abbe56e057f20f883e', '45645646@qq.com', '0000000000');
INSERT INTO `t_user` VALUES ('13', '423423', 'e10adc3949ba59abbe56e057f20f883e', '432423', '0000000000');
INSERT INTO `t_user` VALUES ('14', 'zhangchangyin', 'e10adc3949ba59abbe56e057f20f883e', '12456@qqq.cin', '0000000000');
INSERT INTO `t_user` VALUES ('15', 'test111', 'e10adc3949ba59abbe56e057f20f883e', '446691726@qq.com', '0000000000');
INSERT INTO `t_user` VALUES ('16', 'testttt', 'e10adc3949ba59abbe56e057f20f883e', '123456@qq.com', '0000000000');

-- ----------------------------
-- Table structure for yams_shop
-- ----------------------------
DROP TABLE IF EXISTS `yams_shop`;
CREATE TABLE `yams_shop` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` double(100,0) NOT NULL,
  `img` varchar(100) NOT NULL,
  `details` varchar(200) NOT NULL,
  `family` varchar(10) DEFAULT '亚马逊',
  `logo` varchar(20) DEFAULT NULL,
  `link` varchar(200) DEFAULT 'https://www.amazon.cn/',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100060 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yams_shop
-- ----------------------------
INSERT INTO `yams_shop` VALUES ('100001', '小米笔记本pro', '5566', 'img/xiaomi_air.jpg', '小米(MI)Pro 15.6英寸金属轻薄笔记本(i5-8250U 8G 256G PCIE SSD MX150 2G独显 72%NTSC FHD 指纹识别 预装office)深空灰', '亚马逊', 'img/yams.png', 'https://www.amazon.cn/');
INSERT INTO `yams_shop` VALUES ('100002', '松下/冰箱', '3549', 'img/sx_bx.jpg', '松下（Panasonic）318升典雅银 三门风冷无霜冰箱 自由变温室 -3℃微冻保鲜 NR-C320WP-S', '亚马逊', 'img/yams.png', 'https://www.amazon.cn/');
INSERT INTO `yams_shop` VALUES ('100006', '格力空调3P', '8999', 'img/glkt.jpg', '格力（GREE）3匹 臻净爽 一级能效 变频冷暖 智能 静音 立柜式 客厅圆柱空调柜机KFR-72LW/NhIbB1W', '亚马逊', 'img/yams.png', 'https://www.amazon.cn/');
INSERT INTO `yams_shop` VALUES ('100044', 'Apple/ iPad 2018款', '3088', 'img/apple_ipd.jpg', 'Apple/苹果 iPad 2018款 9.7英寸wifi新款平板电脑32G/128G', '亚马逊', 'img/yams.png', 'https://www.amazon.cn/');
INSERT INTO `yams_shop` VALUES ('100050', '海信HZ75E5A/平板电视', '10999', 'img/hisense_tv.jpg', '海信HZ75E5A 75英寸4K高清全面屏 人工智能平板电视', '亚马逊', 'img/yams.png', 'https://www.amazon.cn/');
INSERT INTO `yams_shop` VALUES ('100052', 'Haier/海尔/热水器', '1699', 'img/haier_water.jpg', 'Haier/海尔 EC8002-MC5电热水器80升家用速热储水洗澡卫生间智能', '亚马逊', 'img/yams.png', 'https://www.amazon.cn/');
INSERT INTO `yams_shop` VALUES ('100059', 'Apple/苹果 iPhone X', '6828', 'img/iphonex.jpg', 'Apple/苹果 iPhone X 苹果x 苹果10 iphonex iphone xs max', '亚马逊', 'img/yams.png', 'https://www.amazon.cn/');
