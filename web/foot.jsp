<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
		<div class="container footer" style="margin-top:50px ;">
			<div class="col-xs-12 col-md-3">
				<img src="img/logo.png"><br/>
				<h3>购物先上比价商城</h3>
			</div>
			<div class="col-xs-12 col-md-7">
				<sapn style="color:gray;font-size:24px">Cheap Shopping</sapn><br><br>
				比价商城是一个倡导理性消费的导购平台，一直以来我们专注为用户推荐高性价比的商品，同时是多功能的购物助手，力求帮助消费者花更少的钱买到优质的商品。
				<br><br><br>合作商城：
				<a href="#">京东</a>
				<a href="#">天猫 </a>
				<a href="#">苏宁</a>
				<a href="#">亚马逊</a>
				<a href="#">国美 </a>
				<a href="#">考拉</a>
				<a href="#">1号店</a>
				<a href="#"> 唯品会</a>
				<a href="#">网易严选</a>
				<a href="#">更多>></a>
			</div>
			<div class="col-xs-12 col-md-2">
				<ul class="win">
					<h4>商务合作</h4>
					<li style="margin-top:5px"><a href="#">B2C商城入驻>></a></li>
					<li style="margin-top:5px"><a href="#">比价API合作>></a></li>
					<li style="margin-top:5px"><a href="#">电商活动报名>></a></li>
					<li style="margin-top:5px"><a href="#">联系我们>></a></li>
				</ul>
			</div>
		</div>
		<div class="link">
			<div class="container">
			
			</div>
		</div>
		<div class="footer_link">
			<div class="container">
				<div class="row">
				&copy;2018
				<a href="#">比价商城</a>
				<em>版权所有</em>
				<em>|</em>
				<a href="#">关于我们</a>
				<em>|</em>
				<a href="#">比价网</a>
				<em>|</em>
				<a href="#">全网评价</a>
				<em>|</em>
				<a href="#">值得买</a>
				<em>|</em>
				<a href="#">历史价格</a>
				<em>|</em>
				<a href="#">友情链接</a>
				<em>|</em>
				<a href="#">品牌大全</a>
				<em>|</em>
				<a href="#">网址导航</a>
				<em>|</em>
				<a href="#">川ICP备123456号</a>
			</div>
			</div>
		</div>