<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		  <title>比价商城</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
		  <script src="js/jquery-1.8.3.min.js"></script>
		  <script src="js/jquery.js"></script>
		  <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		  <script src="js/index.js"></script>
		  <link href="css/index.css" rel="stylesheet">
		  <link rel="icon" href="img/favicon.ico"type="image/x-icon" />
	</head>
	<body>
		
<div class="global-nav">
    <div class="container">
    	<div class="global-nav-left col-xs-4 col-md-3">
    		<a href="#">欢迎登录比价商城</a>
    	</div>
    	<div class="col-md-5"></div>
        <div class="global-nav-right col-md-4 col-xs-8 top">
            <div class="dropdown">
                <a href="#" class="dropdown-main"> <i class="ico helper-sprite-menu"></i> <span>网站导航</span> <i class="ico helper-sprite-dropdown"></i> </a>
                <div class="dropdown-sub col-md-12" style="display:none;">
                    <div class="dropdown-sub_box">
                        <div class="menu_nav">
                            <p class="menu_nav_p1">
                                <span><a target="_blank" href="#">餐饮小吃</a></span>
                                <a target="_blank" href="#">火锅</a>
                                <a target="_blank" href="#">韩式餐饮</a>
                                <a target="_blank" href="#">烤鱼</a>
                                <a target="_blank" href="#">香锅</a>
                                <a target="_blank" href="#">快餐</a>
                                <a target="_blank" href="#">饮品</a>
                                <a target="_blank" href="#">小吃</a>
                                <a target="_blank" href="#">面食</a>
                            </p>
                            <p class="menu_nav_p2">
                                <span><a target="_blank" href="#">饰品礼品</a></span>
                                <a target="_blank" href="#">DIY礼品</a>
                                <a target="_blank" href="#">工艺礼品</a>
                                <a target="_blank" href="#">节庆礼品</a>
                                <a target="_blank" href="#">生活超市</a>
                            </p>
                            <p class="menu_nav_p3">
                                <span><a target="_blank" href="#">服装鞋帽</a></span>
                                <a target="_blank" href="#">男士专属</a>
                                <a target="_blank" href="#">精品折扣</a>
                                <a target="_blank" href="#">精品童装</a>
                                <a target="_blank" href="#">品牌服装</a>
                                <a target="_blank" href="#">女装</a>
                                <a target="_blank" href="#">日韩服饰</a>
                            </p>
                            <p class="menu_nav_p4">
                                <span><a target="_blank" href="#">家居建材</a></span>
                                <a target="_blank" href="#">家居日用</a>
                                <a target="_blank" href="#">装饰装修</a>
                                <a target="_blank" href="#">室内环保</a>
                                <a target="_blank" href="#">特色家居</a>
                                <a target="_blank" href="#">建筑贴膜</a>
                                <a target="_blank" href="#">灯具灯箱</a>
                            </p>
                            <p class="menu_nav_p5">
                                <span><a target="_blank" href="#">环保机械</a></span>
                                <a target="_blank" href="#">节能环保</a>
                                <a target="_blank" href="#">节能设备</a>
                                <a target="_blank" href="#">专利技术</a>
                                <a target="_blank" href="#">机械设备</a>
                                <a target="_blank" href="#">汽车服务</a>
                                <a target="_blank" href="#">设备养护</a>
                                <a target="_blank" href="#">室内排毒</a>
                            </p>
                            <p class="menu_nav_p6">
                                <span><a target="_blank" href="#">教育网络</a></span>
                                <a target="_blank" href="#">自助建站</a>
                                <a target="_blank" href="#">网络商城</a>
                                <a target="_blank" href="#">网上创业</a>
                                <a target="_blank" href="#">教育培训</a>
                                <a target="_blank" href="#">学生用品</a>
                                <a target="_blank" href="#">幼儿早教</a>
                                <a target="_blank" href="#">少儿教育</a>
                            </p>
                            <p class="menu_nav_p7">
                                <span><a target="_blank" href="#">美容保健</a></span>
                                <a target="_blank" href="#">美容</a>
                                <a target="_blank" href="#">化妆品</a>
                                <a target="_blank" href="#">养生馆</a>
                                <a target="_blank" href="#">美体瘦身</a>
                                <a target="_blank" href="#">保健用品</a>
                            </p>
                            <p class="menu_nav_p8">
                                <span><a target="_blank" href="#">特色项目</a></span>
                                <a target="_blank" href="#">新奇特</a>
                                <a target="_blank" href="#">生活服务</a>
                                <a target="_blank" href="#">数码娱乐</a>
                                <a target="_blank" href="#">休闲娱乐</a>
                                <a target="_blank" href="#">娱乐游戏</a>
                                <a target="_blank" href="#">电影院</a>
                                <a target="_blank" href="#">儿童乐园</a>
                                <a target="_blank" href="#">文具店</a>
                            </p>
                        </div>
                    </div>
                </div> 
            </div>
                 <% //判断用户是否登录过
					String login=(String)session.getAttribute("login");
					if(login==null){
				%>
						<span><a style="line-height:30px;margin-left:20px;float:left;color:#666666;" href="${pageContext.request.contextPath}/login.jsp" >登录</a>&nbsp;</span>
	                	<span><a style="line-height:30px;margin-left:20px;float:left;color:#666666;" href="${pageContext.request.contextPath}/register.jsp" >注册会员</a></span>
				<%
					}
					else
					{
				 %>
						<span style="line-height:30px;">欢迎 ${user.username}登录 <a href="${pageContext.request.contextPath}/LogoutServlet">注销</a></span>
				<%
					}
				%>
        </div>
    </div>
</div>
			<!--
            	作者：446691726@qq.com
            	时间：2018-10-21
            	描述：logo
            -->
            <div class="container">
			<div class="row">
				<div class="col-md-2">
					<img src="img/logo.png">
				</div>
				<div class="col-md-2"></div>
				<div class="col-md-5" style="margin-top: 30px;">
						<form action="${pageContext.request.contextPath}/SearchServlet" method="post">
							<div class="row">
								<div class="col-md-12 input-group">
									<input type="text" name="search" class="form-control" placeholder="请输入你想比较的商品名称">
									<span class="input-group-btn">
										<button class="btn btn-warning">
											<span class="glyphicon glyphicon-search"></span>
										</button>
									</span>
								</div>
							</div>
						</form>
						<div class="row" style="font-size: 12px;">
							<div class="col-md-12">
								热门比价:
								&nbsp;<sapn onclick="search(this)"><a href="#">空调</a></sapn>
								&nbsp;<sapn onclick="search(this)"><a href="#">小米</a></sapn>
								&nbsp;<sapn onclick="search(this)"><a href="#">iphone X</a></sapn>
								&nbsp;<sapn onclick="search(this)"><a href="#">冰箱</a></sapn>
								&nbsp;<sapn onclick="search(this)"><a href="#">电视</a></sapn>
								&nbsp;<sapn onclick="search(this)"><a href="#">笔记本</a></sapn>
								&nbsp;<sapn onclick="search(this)"><a href="#">海尔</a></sapn>
								&nbsp;<sapn onclick="search(this)"><a href="#">笔记本</a></sapn>
								&nbsp;<sapn onclick="search(this)"><a href="#">sony</a></sapn>
							</div>
						</div>
					</div>
				<div class="col-md-3"></div>
			</div>
			<!--
            	作者：446691726@qq.com
            	时间：2018-10-21
            	描述：nav导航
            -->
            </div>
            <div class="head_nav">
            	<div class="container">
					<div class="row">
						<ul class="col-md-12">
							<li class="navbar-text"><a href="index.jsp">首页</a></li>
							<li class="navbar-text" onclick="search(this)"><a href="#">电视</a></li>
							<li class="navbar-text" onclick="search(this)"><a href="#">空调</a></li>
							<li class="navbar-text" onclick="search(this)"><a href="#">冰箱</a></li>
							<li class="navbar-text" onclick="search(this)"><a href="#">笔记本</a></li>
							<li class="navbar-text" onclick="search(this)"><a href="#">苹果</a></li>
							<li class="navbar-text" onclick="search(this)"><a href="#">IPad</a></li>
							<li class="navbar-text" onclick="search(this)"><a href="#">热水器</a></li>
							<li class="navbar-text" onclick="search(this)"><a href="#">手机</a></li>
						</ul>
					</div>
				</div>
			</div>
	</body>
</html>
	<script>
		function search(obj){
			var search = $(obj).text();
			window.location.href="${pageContext.request.contextPath}/CompareServlet?search="+search;
		}
	</script>