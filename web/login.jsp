<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<jsp:include page="head.jsp"></jsp:include>
		<div class="container login">
			<div class="row">
				<div class="col-xs-12 col-lg-12">
					<h4 class="zc">用户登录</h4>
				</div>
				<font color="red"></font>
				<form action="${pageContext.request.contextPath}/LoginServlet" method="post">
	            	<div class="col-lg-4"></div>
		    			<div class="col-lg-4">
	    					<div class="row form-group-lg">
		    					<label style="line-height: 40px;" class="col-lg-3 col-xs-3 control-label">用户名:</label>
		    					<div class="col-lg-9 col-xs-9">
		    						<input class="form-control" style="height: 40px;" type="text" name="username" placeholder="请输入2-8位字母数字组合" required/>
		    					</div>
		    				</div>
	    					<div class="row form-group-lg">
		    					<label style="line-height: 40px;" class="col-lg-3 col-xs-3 control-label">密码:</label>
		    					<div class="col-lg-9 col-xs-9">
		    						<input class="form-control" style="height: 40px;" type="password" placeholder="请输入密码" name="password" required/>
			    				</div>
	    					</div>
	    					<div class="row form-group-lg">
            					<div class="col-lg-3 col-xs-3"></div>
            					<div class="col-lg-9 col-xs-9">
            						<input type="submit" class="btn btn-default" value="登陆"/>&nbsp;&nbsp;
            						<a href="register.jsp">
                        				<input type="button" class="btn btn-success" value="立即注册"/></a>
            					</div>
            				</div>
	    				</div>
	    				<div class="col-lg-4"><p style="color:red">${message}</p></div>
	    			</div>
           		</form>
			<div class="row">
				<div style="border-top: 1px dashed #28A4C9;" class="col-xs-12 col-lg-12">
					<img src="img/lc2.jpg" width="1100px" height="80px">
				</div>
			</div>
		</div>
		<jsp:include page="foot.jsp"></jsp:include>