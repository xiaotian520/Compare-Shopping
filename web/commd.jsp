<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<body>
				<jsp:include page="head.jsp"></jsp:include>
		<!--
        	作者：446691726@qq.com
        	时间：2018-10-22
        	描述：商品详情页
       -->
      <c:forEach items="${commd }" var="commd">
		<div class="container"style="box-shadow:0px 0px 20px #9f9f9f; height: 300px; margin-top: 20px;background-color:white;">
			<div class="row">
				<div class="col-md-12">
					<h2>${commd.name }</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-3">
					<img src="${commd.img }" height="200px" width="300px">
				</div>
				<div class="col-xs-12 col-md-5 desc">
					<span>${commd.details }</span>
					<span>上市时间:<%=new Date().toLocaleString() %></span>
				</div>
				<div class="col-xs-12 col-md-2" style="font-size: 20px;margin-top: 50px;">
					<p>参考价:<span>${commd.price }</span></p>
					<p>优选评价:7729条</p>
					<p>类目排名第<i>1</i>位</p>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
		 </c:forEach>
		
		<div class="container" style="margin-top:20px;background-color:white;box-shadow:0px 0px 20px #9f9f9f;">
			<div class="row"><h3>商品对比</h3></div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>商城</th>
						<th>商品名称</th>
						<th>图片</th>
						<th>商品详情</th>
						<th>价格</th>
						<th>商城购买</th>
					</tr>
					<c:if test="${empty commds}">
					  	<span style="color:red;font-size:18px;">${errMsg }</span>
					</c:if>
				</thead>
				<tbody>
				 <c:forEach items="${commds }" var="commds">
					 <tr>
						<td><img src="${commds.logo }"><a href="#">${commds.family}</a></td>
						<td>${commds.name }</td>
						<td><img src="${commds.img }" width="50px" height="50px"></td>
						<td>${commds.details}</td>
						<td>￥${commds.price }</td>
						<td><button class="btn btn-primary"><a style="text-decoration:none;color:white" href="${commds.link }">去购买</a></button></td>
					</tr>
				 </c:forEach>
				</tbody>
			</table>
		</div>
		<jsp:include page="foot.jsp"></jsp:include>
		