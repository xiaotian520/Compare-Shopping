<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<jsp:include page="head.jsp"></jsp:include>
		<div class="container register">
			<div class="row">
				<div class="col-xs-12 col-lg-12">
						<h2 class="zc">新用户注册</h2>
				</div>
				<div class="row">
					<form action="${pageContext.request.contextPath}/RegisterServlet" method="post">
            			<div class="col-lg-2"></div>
            			<div class="col-lg-8">
            				<div class="row form-group-lg">
            					<label style="line-height: 40px;" class="col-lg-3 col-xs-3 control-label">用户名:</label>
            					<div class="col-lg-7 col-xs-7">
            						<input class="form-control" style="height: 40px;" type="text" name="username" value="${uf.username}" placeholder="请输入5-15位字母数字组合" required/>
            					</div>
            					<div class="col-lg-2 col-xs-2"><font color="red">${uf.err['username'] }</font></div>
            				</div>
            				<div class="row form-group-lg">
            					<label style="line-height: 40px;" class="col-lg-3 col-xs-3 control-label">密码:</label>
            					<div class="col-lg-7 col-xs-7">
            						<input class="form-control" style="height: 40px;" type="password" value="${uf.password}" placeholder="请输入6-12位密码" name="password" required/>
            					</div>
            					<div class="col-lg-2 col-xs-2"><font color="red">${uf.err['password'] }</font></div>
            				</div>
            				<div class="row form-group-lg">
            					<label style="line-height: 40px;" class="col-lg-3 col-xs-3 control-label">重复密码:</label>
            					<div class="col-lg-7 col-xs-7">
            						<input class="form-control" style="height: 40px;" type="password" value="${uf.repassword}" placeholder="请重复输入密码" name="repassword" required/>
            					</div>
            					<div class="col-lg-2 col-xs-2"><font color="red">${uf.err['repassword'] }</font></div>
            				</div>
            				<div class="row form-group-lg">
            					<label style="line-height: 40px;" class="col-lg-3 col-xs-3 control-label">邮箱:</label>
            					<div class="col-lg-7 col-xs-7">
            						<input class="form-control" style="height: 40px;" type="email"  value="${uf.email}" placeholder="请输入常用邮箱" name="email" required/>
            					</div>
            					<div class="col-lg-2 col-xs-2"><font color="red">${uf.err['email'] }</font></div>
            				</div>
            				<div class="row form-group-lg">
            					<label style="line-height: 40px;" class="col-lg-3 col-xs-3 control-label">验证码:</label>
            					<div class="col-lg-7 col-xs-7">
            						<div class="row">
            							<div class="col-lg-8 col-xs-8">
            								<input class="form-control" style="height: 40px;" type="text"  value="" placeholder="请输入验证码" name="code" required/>
            							</div>
            							<div class=col-lg-4 col-xs-4">
											<img alt="验证码" onClick="refresh()" id="validatecode" src="${pageContext.request.contextPath}/ValidateCodeServlet">
										</div>
            						</div>
            					</div>
            					<div class="col-lg-2 col-xs-2"><font color="red">${errMsg}</font></div>
            				</div>
            				<div class="row form-group-lg">
            					<div class="col-lg-3 col-xs-3"></div>
            					<div class="col-lg-9 col-xs-9">
            						<input type="submit" class="btn btn-default" value="注册"/>&nbsp;&nbsp;
            						<a href="index.jsp">
                        				<input type="button" class="btn btn-default" value="返回"/></a>
            					</div>
            				</div>
            			</div>
            			<div class="col-lg-2"></div>
           			</form>
				</div>
			</div>
			<div class="row">
				<div style="border-top: 1px dashed #28A4C9;" class="col-xs-12 col-lg-12">
					<img src="img/lc2.jpg" width="1100px" height="80px">
				</div>
			</div>
		</div>
		<jsp:include page="foot.jsp"></jsp:include>