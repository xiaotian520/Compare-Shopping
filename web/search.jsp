<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<style>
			.thumbnail{
				width: 280px;
				height: 410px;
			}
			.text-danger{
				color: red;
			}
		</style>
    <jsp:include page="head.jsp"></jsp:include>
    	<div class="container">
    	<c:if test="${empty Commds}">
    	<span style="color:red;font-size:18px;">${errMsg }</span>
		</c:if>
    	 <c:forEach items="${Commds }" var="commd">
    		<div class="row pull-left">
    			<div class="col-md-4">
    				<div class="thumbnail">
    					<img  src="${commd.img }" width="150px" height="150px" />
    					<div class="caption">
    						<h3>${commd.family }</h3>
    						<h4>${commd.name }</h4>
    						<h5 class="text-danger">￥${commd.price }</h5>
    						<p>${commd.details }</p>
    						<p><a href="${commd.link }" class="btn btn-primary">去购买</a></p>
    					</div>
    				</div>
    			</div>
    		</div>
    	</c:forEach>
    	</div>
    	<jsp:include page="foot.jsp"></jsp:include>
    	<script type="text/javascript">