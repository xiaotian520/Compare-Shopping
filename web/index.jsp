<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" type="text/css" href="css/partialviewslider.min.css">

<c:if test="${empty commd}">
<jsp:forward page="ShowCommdServlet"></jsp:forward> 
</c:if>
	<body>
		<jsp:include page="head.jsp"></jsp:include>
	<!--
       	作者：446691726@qq.com
                   时间：2018-10-21
                 描述：轮播图
    -->
      <div class="container" style="width:77%;max-width:1300px;">
		<ul class="col-md-12" id="partial-view">
			<li>
			  <img src="img/1.png" />
			</li>
			<li>
			  <img src="img/2.png" />
			</li>
			<li>
			  <img src="img/3.png" />
			</li>
	  	</ul>
	 </div>
	<script src="js/partialviewslider.min.js"></script>
	<script>
	$(document).ready(function(){
		var partialView = $('#partial-view').partialViewSlider();
	});
	</script>
		<!--
        	作者：446691726@qq.com
        	时间：2018-10-23
        	描述：商品
        -->
        <div class="container">
        	<div class="row">
        		<h3>&nbsp;&nbsp;&nbsp;京东推荐</h3>
        	</div>
        	<div class="row">
        		<div class="col-md-12">
        			<ul class="hotCommds">
        				<c:forEach items="${commd }" var="commd">
        				<a href="${commd.link }">
        				<li style="float:left;margin-left:17px;height:280px;width:205px;box-shadow:-5px 5px 20px #9f9f9f;background-color:white;margin-top:30px;text-align:center;">
        					<img style="padding-top:20px;" src="${commd.img }" height="150px" width="150px">
        					<h4 class="title" style="height:30px;width:200px;padding-top:20px;"><b>${commd.name }</b></h4>
        					<p style="font-size:20px; height:30px;width:200px;color:red;padding-top:20px;">￥${commd.price }</p>
        				</li>
        				</a>
        				</c:forEach>
        			</ul>
        		</div>
        	</div>
        </div>
	<jsp:include page="foot.jsp"></jsp:include>