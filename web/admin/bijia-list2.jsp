<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
          比价商城后台
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/css/x-admin.css" media="all">
    </head>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
              <a><cite>首页</cite></a>
              <a><cite>产品管理</cite></a>
             
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right"  href="${pageContext.request.contextPath}/CommdListServlet" title="刷新"><i class="layui-icon" style="line-height:30px">ဂ</i></a>
        </div>
        <div class="x-body">
            <form class="layui-form x-center" action="${pageContext.request.contextPath}/SearchCommdServlet" method="post" style="width:80%">
                <div class="layui-form-pane" style="margin-top: 15px;">
                  <div class="layui-form-item">
                    <label class="layui-form-label">查找商品</label>
                    <div class="layui-input-inline">
                      <input type="text" class="layui-input" placeholder="商品名" name="name">
                    </div>
                    <lable class="layui-form-label">价格区间</lable>
                    <div class="layui-input-inline">
                     <input type="text"  class="layui-input" placeholder="最低价" name="minprice" />
                    </div>
                    <div class="layui-input-inline">
                     <input type="text" class="layui-input" placeholder="最高价" name="maxprice"/>
                    </div>
                    <div class="layui-input-inline" style="width:80px">
                        <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                    </div>
                  </div>
                </div> 
                </form>
            <button class="layui-btn" onclick="admin_add('添加商品','${pageContext.request.contextPath}/admin/bijia-add.jsp','600','500')"><i class="layui-icon">&#xe608;</i>添加</button></xblock>
            <xblock><table class="layui-table">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            产品名
                        </th>
                        <th>
                            价格
                        </th>
                        <th>
                            图片
                        </th>
                        <th>
                            详情
                        </th>
                        <th>
                            商城
                        </th>
                        </th>
                        <th>
                            操作
                        </th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${commd}" var="commd">
                    <tr>
                        <td>
                           ${commd.id}
                        </td>
                        <td>
                           ${commd.name}
                        </td>
                        <td >
                           ${commd.price}
                        </td>
                        <td >
                           <img src="${commd.img}" width="50px" height="50px">
                        </td>
                        <td >
                           ${commd.details}
                        </td>
                        <td>
                            ${commd.family}
                        </td>
                        
                        <td class="td-manage">
                            <a title="编辑" href="javascript:;" onclick="admin_edit('编辑','${pageContext.request.contextPath}/FindCommdByIdServlet?id=${commd.id}','4','','510')"
                            class="ml-5" style="text-decoration:none">
                                <i class="layui-icon">&#xe642;</i>
                            </a>
                            <a title="删除" style="text-decoration:none" href="javascript:deleteCommd('${commd.name}','${commd.id}')">
                            <i class="layui-icon">&#xe640;</i>
							</a>
                        </td>
                    </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <script src="./admin/lib/layui/layui.js" charset="utf-8"></script>
        <script src="./admin/js/x-layui.js" charset="utf-8"></script>
        <script>
            layui.use(['laydate','element','laypage','layer'], function(){
                $ = layui.jquery;//jquery
              lement = layui.element();//面包导航
              layer = layui.layer;//弹出层

              //以上模块根据需要引入
            });

             /*添加*/
            function admin_add(title,url,w,h){
                x_admin_show(title,url,w,h);
            }
            //编辑
            function admin_edit (title,url,id,w,h) {
                x_admin_show(title,url,w,h); 
            }
        	//删除
        	function deleteCommd(Commdname,CommdId) {
        		//确认提示框
        		if(confirm('是否要删除【' + Commdname + '】 这件商品?')){
        			//确定，要删除
        			location.href = '${pageContext.request.contextPath}/DeleteCommdByIdServlet?id=' + CommdId;
        		}else{
        			//不删除
        		}
        	}
            </script>
            <script>
        var _hmt = _hmt || [];
        (function() {
          var hm = document.createElement("script");
          hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
          var s = document.getElementsByTagName("script")[0]; 
          s.parentNode.insertBefore(hm, s);
        })();
        </script>
    </body>
</html>