<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
          比价商城后台
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/css/x-admin.css" media="all">
    </head>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
              <a><cite>首页</cite></a>
              <a><cite>会员管理</cite></a>
              <a><cite>会员列表</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right"  href="${pageContext.request.contextPath}/AdminUserListServlet" title="刷新"><i class="layui-icon" style="line-height:30px">ဂ</i></a>
        </div>
        <div class="x-body">
            <form class="layui-form x-center" action="${pageContext.request.contextPath}/AdminSearchUserServlet" method="post" style="width:800px">
               <div class="layui-form-pane" style="margin-top: 15px;">
                  <div class="layui-form-item layui-inline">
                    <label class="layui-form-label">查找用户</label>
                    <div class="layui-input-inline">
                      	<input type="text" class="layui-input" placeholder="用户名" name="username">
                    </div>
                    <div class="layui-input-inline">
                     	<input type="text"  class="layui-input" placeholder="用户类型" name="type" />
                    </div>
                    <div class="layui-input-inline" style="width:80px">
                        <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                    </div>
                  </div>
                </div> 
            </form>
            <xblock><button class="layui-btn" onclick="member_add('添加用户','${pageContext.request.contextPath}/admin/member-add.jsp','600','500')"><i class="layui-icon">&#xe608;</i>添加</button></xblock>
            <table class="layui-table">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            用户名
                        </th>
                        <th>
                        密码
                        </th>
                        <th>
                            邮箱
                        </th>
                        <th>
                        用户类型
                        </th>
                        <th>
                            操作
                        </th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>
                            ${user.id}
                        </td>
                        <td>
                             ${user.username}
                        </td>
                        <td>
                        	 ${user.password}
                        </td>
                        <td >
                           ${user.email}
                        </td>
                        <td>
                        ${user.type == 1 ? "管理员":"普通用户" }
                        </td>
                        <td class="td-manage">
                            <a title="编辑" href="javascript:;" onclick="member_edit('编辑','${pageContext.request.contextPath}/AdminFindUserByIdServlet?id=${user.id}','4','','510')"
                            class="ml-5" style="text-decoration:none">
                                <i class="layui-icon">&#xe642;</i>
                            </a>
                            <a title="删除" href="javascript:deleteUser('${user.username}','${user.id}')" style="text-decoration:none">
                                <i class="layui-icon">&#xe640;</i>
                            </a>
                        </td>
                    </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <script src="${pageContext.request.contextPath}/admin/lib/layui/layui.js" charset="utf-8"></script>
        <script src="${pageContext.request.contextPath}/admin/js/x-layui.js" charset="utf-8"></script>
        <script>
            layui.use(['laydate','element','laypage','layer'], function(){
                $ = layui.jquery;//jquery
              lement = layui.element();//面包导航
              layer = layui.layer;//弹出层
            });

             /*用户-添加*/
            function member_add(title,url,w,h){
                x_admin_show(title,url,w,h);
            }
            /*用户-查看*/
            function member_show(title,url,id,w,h){
                x_admin_show(title,url,w,h);
            }

            // 用户-编辑
            function member_edit (title,url,id,w,h) {
                x_admin_show(title,url,w,h); 
            }
            /*密码-修改*/
            function member_password(title,url,id,w,h){
                x_admin_show(title,url,w,h);  
            }
            /*用户-删除*/
        	function deleteUser(Username,UserId) {
       		//确认提示框
       		if(confirm('是否要删除【' + Username + '】 这个用户?')){
       			//确定，要删除
       			location.href = '${pageContext.request.contextPath}/AdminDeleteUserByIdServlet?id=' + UserId;
       		}else{
       			//不删除
       		}
        }
            </script>
            <script>
        var _hmt = _hmt || [];
        (function() {
          var hm = document.createElement("script");
          hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
          var s = document.getElementsByTagName("script")[0]; 
          s.parentNode.insertBefore(hm, s);
        })();
        </script>
    </body>
</html>